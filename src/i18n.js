import i18n from 'i18next'
import Backend from 'i18next-http-backend'
import LanguageDetector from 'i18next-browser-languagedetector'
import { initReactI18next } from 'react-i18next'

import {
   LANGUAGE_STRING,
   IN_DEVELOPMENT_MDOE,
   DEVELOPMENT_LANGUAGE_PATH,
   PRODUCTION_LANGUAGE_PATH,
} from './components/Constants/Enviroment/Enviroment'
i18n
   .use(LanguageDetector)
   .use(Backend)
   .use(initReactI18next)
   // detect user language
   // learn more: https://github.com/i18next/i18next-browser-languageDetector

   // pass the i18n instance to react-i18next.

   .init({
      lng: localStorage.getItem(LANGUAGE_STRING)
         ? localStorage.getItem(LANGUAGE_STRING)
         : 'en',
      fallbackLng: 'en',
      detection: {
         // order and from where user language should be detected
         order: [
            'querystring',
            'cookie',
            'localStorage',
            'sessionStorage',
            'navigator',
            'htmlTag',
            'path',
            'subdomain',
         ],

         // keys or params to lookup language from
         lookupQuerystring: 'lng',
         lookupCookie: 'i18next123',
         lookupLocalStorage: 'i18nextLng',
         // cache user language on
         caches: ['localStorage', 'sessionStorage', 'cookie'],
         // excludeCacheFor: ['cimode'], // languages to not persist (cookie, localStorage)

         // optional expire and domain for set cookie
         cookieMinutes: 10,
         cookieDomain: 'http://localhost:3000',

         // optional htmlTag with lang attribute, the default is:
         htmlTag: document.documentElement,

         // only detect languages that are in the whitelist
         checkWhitelist: false,

         checkForSimilarInWhitelist: false,

         // optional set cookie options, reference:[MDN Set-Cookie docs](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie)
         cookieOptions: { path: '/' },
      },
      debug: true,
      /* can have multiple namespace, in case you want to divide a huge translation into smaller pieces and load them on demand */
      ns: ['translations'],
      defaultNS: 'translations',
      keySeparator: false,
      initImmediate: true,
      backend: {
         /* translation file path */
         loadPath:
            process.env.NODE_ENV === IN_DEVELOPMENT_MDOE
               ? `${DEVELOPMENT_LANGUAGE_PATH}{{ns}}/{{lng}}.json`
               : `${PRODUCTION_LANGUAGE_PATH}{{ns}}/{{lng}}.json`,
         allowMultiLoading: true,
      },
      interpolation: {
         escapeValue: false,
         // formatSeparator: ',',
      },
      react: {
         useSuspense: true,
         // wait: true,
         wait: false,
         bindI18n: 'languageChanged',
         bindI18nStore: '',
         transEmptyNodeValue: '',
         transSupportBasicHtmlNodes: false,
         // transKeepBasicHtmlNodesFor: ['br', 'strong', 'i','option','select'],
      },
      // useSupense: false,
   })
export default i18n
