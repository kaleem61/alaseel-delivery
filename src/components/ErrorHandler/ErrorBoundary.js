import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Card } from 'react-bootstrap'

class ErrorBoundary extends Component {
   constructor(props) {
      super(props)
      this.state = {
         hasError: false,
         info: null,
      }
   }
   static getDerivedStateFromError(error) {
      return { hasError: true }
   }
   componentDidCatch(error, info) {
      this.setState({
         hasError: true,
         info: info,
      })
   }
   render() {
      if (this.state.hasError) {
         return (
            <Card
               className="m-auto"
               style={{
                  textAlign: 'center',
                  background: 'rgb(255, 241, 214)',
               }}
            >
               {'😲 Oops! ⚠️ Something Went Wrong ...'}
               <button
                  className="btn btn-success"
                  onClick={() =>
                     this.setState({
                        hasError: false,
                     })
                  }
               >
                  Click To Continue
               </button>
            </Card>
         )
      }
      return this.props.children
   }
}

export default ErrorBoundary
