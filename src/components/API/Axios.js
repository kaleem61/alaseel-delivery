import axios from 'axios'
import {
   LOCAL_API_URL,
   IN_PRODUCTION_MODE,
   PRODUCTION_API_URL,
   CURRENT_ENVIREMENT,
   IN_STAGGING_MODE,

   STAGGING_API_URL,
} from '../Constants/Enviroment/Enviroment'
const Axios = axios.create({
   baseURL:
      CURRENT_ENVIREMENT === IN_PRODUCTION_MODE
         ? PRODUCTION_API_URL
         : CURRENT_ENVIREMENT === IN_STAGGING_MODE
         ? STAGGING_API_URL
         : LOCAL_API_URL,
})
// Axios.defaults.headers.common['Authorization'] = `bearer ${localStorage.getItem(
//    'authtoken'
// )}`
export default Axios
