import React, { Component } from 'react'
import { Card } from 'react-bootstrap'
// import axios from 'axios'
import axios from '../API/Axios'
import PieClass from '../d3charts/PieClass'
import Map from '../googlemap/Map'
import BoostrapDataTable from '../datatable/Datatable'
import { MQTT } from '../../MQTT'
import cakey from '../../assets/client_key.pem'
import {
   LOCAL_API_URL,
   LANGUAGE_STRING,
} from '../Constants/Enviroment/Enviroment'
import _ from 'lodash'
import {
   col6,
   col12,
   col4,
   col8,
   col3,
   col7,
   col5,
   col2,
   col10,
   col_lg_auto,
   col_sm_auto,
   col_xs_auto,
   col_md_auto,
} from '../Constants/Classes/BoostrapClassses'
import {
   ProgressBar,
   Button,
   FormControl,
   InputGroup,
   OverlayTrigger,
   Tooltip,
} from 'react-bootstrap'
import { ToastContainer, toast, Zoom } from 'react-toastify'
import { Trans } from 'react-i18next'
// import '../../css/ControlTower.css';
import style from './ControlTower.module.css'
import { BounceLoader, ClipLoader } from 'react-spinners'
import { interpolateGreys } from 'd3'
import {
   Paho,
   Client,
   Message,
   onConnect,
   onConnectionLost,
   onMessageArrived,
} from 'paho-mqtt'
import { LANG_AR } from '../Constants/Language/Language'
class Controltower extends Component {
   constructor(props) {
      super(props)
      // this._isMounted=false
      this.state = {
         test: '',
         chartData: null,
         statisticalData: null,
         vehicleRoutes: null,
         orders: [],
         pageloading: true,
         selectedBranchId: null,
         pageBodyStyle: { filter: 'grayscale(100%)', opacity: 0.5 },
         lateDispatchedPercent: 0,
         ordersNotCompleted: 0,
         vehicleTracking: [],
         vehicleCategories: [],
         chartUpdate: false,
         liveVehicles: [],
         liveVehiclesStats: [],
         mapRefreshDisabled: false,
      }
      this._isMounted = false
      this.tdRef = React.createRef()
      this.oncRef = React.createRef()
      this.doRef = React.createRef()
      this.osRef = React.createRef()
      this.vRef = React.createRef()
      this.data = [10, 20, 30, 40, 50]
      this.now = [27, 0, 0, 0]
   }

   // signal = axios.CancelToken.source()

   componentDidMount() {
      this._isMounted = true
      if (this.props.selectedwarehouse_id) {
         this.getTowerStatisticalData()
      }
   }

   componentDidUpdate(prevProps, prevState) {
      let t = this.props.t
      let lang = this.props.language
      if (
         this.props.selectedwarehouse_id &&
         parseInt(this.props.selectedwarehouse_id) !==
            parseInt(prevProps.selectedwarehouse_id)
      ) {
         let availobject = {
            count: 0,
            status: {
               en: 'Available',
               ar: lang === LANG_AR ? t('Available') : 'Available',
            },
         }
         let transitobj = {
            count: 0,
            status: {
               en: 'In Transit',
               ar: lang === LANG_AR ? t('In Transit') : 'In Transit',
            },
         }
         let unavailableObj = {
            count: 0,
            status: {
               en: 'Un Available',
               ar: lang === LANG_AR ? t('Un Available') : 'Un Available',
            },
         }

         const { client } = this.state

         let liveVehicles = [availobject, transitobj, unavailableObj]
         this.setState({
            selectedBranchId: this.props.selectedwarehouse_id,
            statisticalData: null,
            liveVehicles: [],
            // liveVehiclesStats: [],
            vehicleCategories: [],
            vehicleTracking: [],
            liveVehiclesStats: liveVehicles,
         })
         if (typeof client !== 'undefined') {
            if (client.isConnected()) {
               client.disconnect()
            }
         }
         this.getTowerStatisticalData()
      }
      if (this.state.liveVehicles !== prevState.liveVehicles) {
         if (this.state.liveVehicles.length > 0) {
            this.PAHOFUNC(this.state.liveVehicles)
         } else {
            this.showMessage(this.props.t('No Live Vehicle Available'), 'error')
         }
      }
      if (this.state.vehicleTracking !== prevState.vehicleTracking) {
         let transitvehicles = _.filter(
            this.state.vehicleTracking,
            (vehicle, i) => {
               return vehicle.status === 'In Transit'
            }
         )
         let availableVehicles = _.filter(
            this.state.vehicleTracking,
            (vehicle, i) => {
               return vehicle.status === 'Available'
            }
         )
         let UnavailableVehicles = []
         let getAvailableIds = _.map(this.state.vehicleTracking, 'vehicle_id')
         let getofflinevehciles = []
         let liveVehiclesDatArr = [...this.state.liveVehicles]
         liveVehiclesDatArr.map((liveData, key) => {
            if (!getAvailableIds.includes(liveData.vehicle_id.toString())) {
               getofflinevehciles.push(liveData)
            }
         })

         let availobject = {
            count: availableVehicles.length,
            status: {
               en: 'Available',
               ar: lang === LANG_AR ? t('Available') : 'Available',
            },
         }
         let transitobj = {
            count: transitvehicles.length,
            status: {
               en: 'In Transit',
               ar: lang === LANG_AR ? t('In Transit') : 'In Transit',
            },
         }
         let availableObj = {
            count: getofflinevehciles.length,
            status: {
               en: 'Un Available',
               ar: lang === LANG_AR ? t('Un Available') : 'Un Available',
            },
         }

         let liveVehicles = [availobject, transitobj, availableObj]
         this.setState({
            liveVehiclesStats: liveVehicles,
            chartUpdate: !this.state.chartUpdate,
         })
      }
      if (this.props.language !== prevProps.language) {
         let vehicleStats = [...this.state.liveVehiclesStats]
         if (vehicleStats.length > 0) {
            let availStats = _.find(vehicleStats, ({ status }) => {
               return status.en === 'Available'
            })
            let transitStats = _.find(vehicleStats, ({ status }) => {
               return status.en === 'In Transit'
            })

            let unavailAbleStats = _.find(vehicleStats, ({ status }) => {
               return status.en === 'Un Available'
            })

            let availobject = {
               count: availStats.count,
               status: {
                  en: availStats.status.en,
                  ar:
                     lang === LANG_AR
                        ? t(availStats.status.ar)
                        : availStats.status.ar,
               },
            }
            let transitobj = {
               count: transitStats.count,
               status: {
                  en: transitStats.status.en,
                  ar:
                     lang === LANG_AR
                        ? t(transitStats.status.ar)
                        : transitStats.status.ar,
               },
            }
            let unavailableObj = {
               count: unavailAbleStats.count,
               status: {
                  en: unavailAbleStats.status.en,
                  ar:
                     lang === LANG_AR
                        ? t(unavailAbleStats.status.ar)
                        : unavailAbleStats.status.ar,
               },
            }
            let liveVehicles = [availobject, transitobj, unavailableObj]
            this.setState({
               liveVehiclesStats: liveVehicles,
               chartUpdate: !this.state.chartUpdate,
            })
         }
      }
   }
   componentWillUnmount() {
      // this.signal.cancel('Api is being canceled')
      this._isMounted = false
      const { client } = this.state
      if (typeof client !== 'undefined') {
         if (client.isConnected()) {
            client.disconnect()
         }
      }
   }
   getTowerStatisticalData = () => {
      this.setState({
         pageloading: true,
      })
      axios
         .get(
            `storesupervisor/v1/alaseelTower/${this.props.selectedwarehouse_id}`,
            {
               headers: {
                  Authorization: `bearer ${localStorage.getItem('authtoken')}`,
               },
            }
         )
         .then((res) => {
            let response = res.data
            if (response.code === 200) {
               let percentageLateDispatched = 0
               let individualcount = 0
               let totalcount = 0
               if (response.data.cardTwo.length > 0) {
                  let carddata = response.data.cardTwo
                  carddata.map((data) => {
                     if (data.status.en === 'Delayed Orders') {
                        totalcount = totalcount + parseInt(data.count)
                     }
                  })
                  carddata.map((data) => {
                     individualcount = individualcount + parseInt(data.count)
                     // percentageLateDispatched
                  })
                  if (individualcount > 0 && totalcount > 0) {
                     percentageLateDispatched =
                        (individualcount * 100) / totalcount
                  }
               }
               let ordersNotCompleted = 0
               if (response.data.cancelStatistics.length > 0) {
                  let cancelstats = response.data.cancelStatistics
                  cancelstats.map((data) => {
                     ordersNotCompleted =
                        ordersNotCompleted + parseInt(data.count)
                  })
               }
               let t = this.props.t
               let lang = this.props.language
               let vstats = [
                  {
                     count: 0,
                     status: {
                        en: 'Available',
                        ar: lang === LANG_AR ? t('Available') : 'Available',
                     },
                  },
                  {
                     count: 0,
                     status: {
                        en: 'In Transit',
                        ar: lang === LANG_AR ? t('In Transit') : 'In Transit',
                     },
                  },
                  {
                     count: response.data.Vehicles.length,
                     status: {
                        en: 'Un Available',
                        ar:
                           lang === LANG_AR
                              ? t('Un Available')
                              : 'Un Available',
                     },
                  },
               ]
               if (this._isMounted) {
                  this.setState({
                     statisticalData: response.data,
                     lateDispatchedPercent: percentageLateDispatched,
                     ordersNotCompleted: ordersNotCompleted,
                     pageloading: false,
                     pageBodyStyle: {},
                     liveVehicles: response.data.Vehicles,
                     vehicleCategories: response.data.vehicleCategories,
                     liveVehiclesStats: vstats,
                  })
                  // this.tdRef = React.createRef()
                  this.showMessage(
                     this.props.t('Tower Data Loaded Successfully'),
                     'success'
                  )
               }
            }
         })
         .catch((error) => {
            this.showMessage(error.toString(), 'error', false)
         })
   }
   showMessage = (message, type, autoClose = 2000) =>
      toast(message, {
         type: type,
         // autoClose: false,
         autoClose: autoClose,
         className:
            type === 'success'
               ? style.toastContainerSuccess
               : style.toastContainer,
      })

   getDistanceFromLatLonInM = (lat1, lon1, lat2, lon2) => {
      var R = 6371 // Radius of the earth in km
      var dLat = this.deg2rad(lat2 - lat1) // deg2rad below
      var dLon = this.deg2rad(lon2 - lon1)
      var a =
         Math.sin(dLat / 2) * Math.sin(dLat / 2) +
         Math.cos(this.deg2rad(lat1)) *
            Math.cos(this.deg2rad(lat2)) *
            Math.sin(dLon / 2) *
            Math.sin(dLon / 2)
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
      var d = R * c // Distance in km
      return d * 1000
   }

   deg2rad = (deg) => {
      return deg * (Math.PI / 180)
   }

   onRefreshClick = () => {
      this.setState({
         pageloading: true,
         selectedBranchId: this.props.selectedwarehouse_id,
         statisticalData: null,
      })
      this.getTowerStatisticalData()
   }

   PAHOFUNC = (vehicle_id) => {
      let client = new Client(
         'gbhcdemosys.com',
         Number(15676),
         '/ws',
         'mqttjs_' + Math.random().toString(16).substr(2, 8)
         // 'kaleemmqttjs'
      )
      // set callback handlers
      client.connect({
         onSuccess: () => {
            vehicle_id.map(({ vehicle_id }) => {
               client.subscribe(vehicle_id)
               this.setState({
                  client,
                  chartUpdate: !this.state.chartUpdate,
                  pageloading: false,
                  mapRefreshDisabled: false,
               })
            })
         },
         userName: 'admin_aseel',
         password: 'AlAseel!@#',
         // userName: 'guest',
         // password: 'guest',
         mqttVersion: 4,
         useSSL: true,
         cleanSession: false,
         onFailure: (message) => {
            this.setState({
               pageloading: false,
               chartUpdate: !this.state.chartUpdate,
               mapRefreshDisabled: false,
            })
         },
         keepAliveInterval: 15000,
         reconnect: true, // Enable automatic reconnect
         timeout: 15000,
      })
      client.onConnectionLost = (responseObject) => {
         this.setState({
            pageloading: false,
            chartUpdate: !this.state.chartUpdate,
            mapRefreshDisabled: false,
            vehicleTracking: [],
         })
         if (responseObject.errorCode !== 0) {
            console.log('onConnectionLost:' + responseObject.errorMessage)
         }
      }
      client.onMessageArrived = (message) => {
         let source = message.destinationName
         let liveVehicles = [...this.state.liveVehicles]
         let vehiclesArray = _.map(this.state.liveVehicles, ({ vehicle_id }) =>
            parseInt(vehicle_id)
         )
         let messageString = message.payloadString
         this.setState({ pageloading: false })
         let trackingData = [...this.state.vehicleTracking]
         let filterMessage = messageString.split(',')
         let vehilceLatLng = {
            lat: parseFloat(filterMessage[0]),
            lng: parseFloat(filterMessage[1]),
         }

         const checkTopicExist = _.find(vehicle_id, ({ vehicle_id }) => {
            return vehicle_id === source
         })
         if (typeof checkTopicExist !== 'undefined') {
            let InTransit = 'In Transit'
            let Available = 'Available'
            let UnAvailable = 'UnAvailable'
            const isExist = _.find(trackingData, ({ vehicle_id }) => {
               return vehicle_id === source
            })

            let filteredVehicle = null
            if (vehiclesArray.includes(parseInt(source))) {
               filteredVehicle = _.find(liveVehicles, (vehicle) => {
                  let veh_id = vehicle.vehicle_id
                  return parseInt(veh_id) === parseInt(source)
               })
            }
            let vehObj = {
               vehicle_id: null,
               polyLinePath: [],
               vehiclePath: null,
               status: null,
               vehicleCode: null,
               plateNo: null,
               vehicleCategory: null,
               driverName: null,
               driverErpId: null,
               tripCounter: null,
            }
            vehObj.vehicle_id = source
            vehObj.vehiclePath = vehilceLatLng
            if (filteredVehicle) {
               vehObj.vehicleCode = filteredVehicle.vehicle_code
               vehObj.plateNo = filteredVehicle.number_plate
               vehObj.vehicleCategory = filteredVehicle.vehicle_category
               vehObj.driverName = filteredVehicle.driver_name
               vehObj.driverErpId = filteredVehicle.driver_erp_id
               vehObj.tripCounter = filteredVehicle.trips
            }
            vehObj.polyLinePath.push(vehilceLatLng)
            if (typeof isExist === 'undefined') {
               if (filterMessage.length > 0) {
                  trackingData.push(vehObj)
                  this.setState({
                     vehicleTracking: trackingData,
                     chartUpdate: !this.state.chartUpdate,
                  })
               }
            } else {
               trackingData.map(({ vehicle_id, polyLinePath }, key) => {
                  if (vehicle_id === source) {
                     if (filterMessage.length > 0) {
                        trackingData[key].polyLinePath.push(vehilceLatLng)
                        trackingData[key].vehiclePath = vehilceLatLng
                        if (filteredVehicle) {
                           trackingData[key].vehicleCode =
                              filteredVehicle.vehicle_code

                           trackingData[key].plateNo =
                              filteredVehicle.number_plate
                           trackingData[key].vehicleCategory =
                              filteredVehicle.vehicle_category
                           trackingData[key].driverName =
                              filteredVehicle.driver_name
                           trackingData[key].driverErpId =
                              filteredVehicle.driver_erp_id
                           trackingData[key].tripCounter = filteredVehicle.trips
                        }
                        let lastThreeIndex = polyLinePath.slice(
                           polyLinePath.length - 4
                        )

                        let origin = this.props.defaultCenter
                        let differencearr = []
                        lastThreeIndex.map(({ lat, lng }, newkey) => {
                           let difference = this.getDistanceFromLatLonInM(
                              lat,
                              lng,
                              origin.lat,
                              origin.lng
                           )

                           differencearr.push(parseInt(difference))
                        })

                        let max = Math.max(...differencearr)
                        if (max > 111) {
                           trackingData[key].status = InTransit
                        } else {
                           trackingData[key].status = Available
                        }

                        // console.log('Check Tracking Data', trackingData)
                        this.setState({
                           vehicleTracking: trackingData,
                           chartUpdate: !this.state.chartUpdate,
                           mapRefreshDisabled: false,
                           // liveVehiclesStats: liveVehicles,
                        })
                     }
                  }
               })
            }
         } else {
            console.log('Form Unknown Source')
         }
      }
      client.onConnect = (message) => {
         console.log('check test log', message)
      }
      client.onConnected = (resObj) => {
         this.setState({
            pageloading: false,
            chartUpdate: !this.state.chartUpdate,
            mapRefreshDisabled: false,
         })
         console.log('Connected to', resObj)
         // client.subscribe('World')
      }
   }
   onRefreshMapClick = () => {
      this.setState({
         chartUpdate: !this.state.chartUpdate,
         mapRefreshDisabled: true,
      })
      let veh_id = ['111', '333']
      if (this.state.liveVehicles.length > 0) {
         this.PAHOFUNC(this.state.liveVehicles)
      } else {
         this.showMessage(this.props.t('No Live Vehicle Available'), 'error')
      }
   }
   render() {
      let t = this.props.t
      let lang = this.props.language
      return (
         <React.Fragment>
            <ClipLoader
               css={`
                  position: fixed;
                  top: 40%;
                  left: 42%;
                  right: 40%;
                  bottom: 20%;

                  // opacity: 0.5;
                  z-index: 999999;
                  background-color: grey;
               `}
               size={'200px'}
               this
               also
               works
               color={'#196633'}
               height={100}
               loading={this.state.pageloading}
            />
            <div className={this.state.pageloading ? style.loadmain : null}>
               <ToastContainer
                  transition={Zoom}
                  position="top-center"
                  hideProgressBar={false}
                  newestOnTop={false}
                  closeOnClick
                  rtl={false}
                  pauseOnVisibilityChange
                  draggable
                  pauseOnHover
               />

               <div className="row mt-2 no-gutters">
                  <div className={col6}>
                     <div className="row">
                        <div
                           className={`${col_sm_auto} ${col_xs_auto} ${col_md_auto} ${col_lg_auto}`}
                        >
                           <b> {t('Delivery Leg Details')}</b>
                        </div>{' '}
                        <div className={col2}>
                           <OverlayTrigger
                              key={1}
                              placement={'right'}
                              overlay={
                                 <Tooltip
                                    id={`tooltip-12`}
                                    style={{ fontSize: '10px' }}
                                 >
                                    {t('Refresh Page Content')}
                                 </Tooltip>
                              }
                           >
                              <i
                                 className="fa fa-refresh"
                                 onClick={this.onRefreshClick}
                              ></i>
                           </OverlayTrigger>
                        </div>
                     </div>
                  </div>
                  <div className={col6}>
                     <div
                        className={`${col_sm_auto} ${col_xs_auto} ${col_md_auto} ${col_lg_auto}`}
                     >
                        <OverlayTrigger
                           key={1}
                           placement={'right'}
                           overlay={
                              <Tooltip
                                 id={`tooltip-123`}
                                 style={{ fontSize: '10px' }}
                              >
                                 {t('Refresh Map')}
                              </Tooltip>
                           }
                        >
                           <Button
                              size="sm"
                              disabled={this.state.mapRefreshDisabled}
                              variant="secondary"
                              className={style.refreshButton}
                              onClick={this.onRefreshMapClick}
                           >
                              {t('Refresh Map')}
                           </Button>
                        </OverlayTrigger>
                     </div>
                  </div>
               </div>
               <div className="row">
                  <div className={`${col6} ${style.cardWrapper}`}>
                     <div className="row mt-1">
                        <div className={col6}>
                           <Card>
                              <div
                                 className={`${
                                    lang === LANG_AR && 'text-right'
                                 } row`}
                                 dir={lang === LANG_AR ? 'rtl' : null}
                              >
                                 <div
                                    className={`${col6} ${
                                       lang === LANG_AR ? 'mr-2' : 'ml-2'
                                    } `}
                                 >
                                    <small className={style.textFontSize}>
                                       {t('Total Deliveries')}
                                    </small>
                                 </div>
                                 <div className={`${col5} text-right`}>
                                    <small className={style.textFontSize}>
                                       {this.state.statisticalData
                                          ? this.state.statisticalData
                                               .deliveries
                                          : null}
                                    </small>
                                 </div>
                              </div>
                              <div
                                 className={`${
                                    lang === LANG_AR && 'text-right'
                                 } row no-gutters`}
                                 dir={lang === LANG_AR ? 'rtl' : null}
                              >
                                 <div
                                    className={`${col7} ${
                                       lang === LANG_AR ? 'mr-2' : 'ml-2'
                                    } `}
                                 >
                                    <OverlayTrigger
                                       key={1}
                                       placement={'top'}
                                       overlay={
                                          <Tooltip
                                             id={`tooltip-1234`}
                                             style={{ fontSize: '10px' }}
                                          >
                                             {t('Orders-Not Dispatched')}
                                          </Tooltip>
                                       }
                                    >
                                       <small className={style.textFontSize}>
                                          {this.state.statisticalData
                                             ? parseInt(
                                                  this.state.statisticalData
                                                     .notDispatchedPercent
                                               )
                                             : 0}
                                          % {t('Orders-Not Dispatched')}
                                       </small>
                                    </OverlayTrigger>{' '}
                                 </div>
                                 <div className={col4}>
                                    <ProgressBar
                                       className={`${style.progress}`}
                                       now={
                                          this.state.statisticalData
                                             ? this.state.statisticalData
                                                  .notDispatchedPercent
                                             : 0
                                       }
                                    />
                                 </div>
                              </div>
                              <hr></hr>
                              <Card.Body>
                                 <div className="row" ref={this.tdRef}>
                                    <PieClass
                                       data={
                                          this.state.statisticalData
                                             ? this.state.statisticalData
                                                  .orders_counters
                                             : []
                                       }
                                       update={this.state.chartUpdate}
                                       lang={this.props.language}
                                       colorData={[
                                          '#2EA6F9',
                                          '#39C86A',
                                          '#E54515',
                                          '#866417',
                                          '#F99D2E',
                                          '#d19e2b',
                                       ]}
                                       legendRef={this.tdRef}
                                    />
                                 </div>
                              </Card.Body>
                           </Card>
                        </div>
                        <div className={col6}>
                           <Card>
                              <div
                                 className={`${
                                    lang === LANG_AR && 'text-right'
                                 } row`}
                                 dir={lang === LANG_AR ? 'rtl' : null}
                              >
                                 <div
                                    className={`${col6}  ${
                                       lang === LANG_AR ? 'mr-2' : 'ml-2'
                                    } `}
                                 >
                                    <small className={style.textFontSize}>
                                       {'   '}
                                       {t('Delayed Orders')}
                                    </small>
                                 </div>
                                 <div className={`${col5} text-right`}>
                                    <span className={style.textFontSize}>
                                       {this.state.statisticalData
                                          ? this.state.statisticalData
                                               .cardTwo[0].count
                                          : 0}
                                    </span>
                                 </div>
                              </div>
                              <div
                                 className={`${
                                    lang === LANG_AR && 'text-right'
                                 } row no-gutters`}
                                 dir={lang === LANG_AR ? 'rtl' : null}
                              >
                                 <div
                                    className={`${col7} ${
                                       lang === LANG_AR ? 'mr-2' : 'ml-2'
                                    } `}
                                    // title="Orders-Late Dispatched"
                                 >
                                    <OverlayTrigger
                                       key={1}
                                       placement={'top'}
                                       overlay={
                                          <Tooltip
                                             id={`tooltip-12345`}
                                             style={{ fontSize: '10px' }}
                                          >
                                             {t('Orders-Late Dispatched')}
                                          </Tooltip>
                                       }
                                    >
                                       <small className={style.textFontSize}>
                                          {' '}
                                          {
                                             this.state.lateDispatchedPercent
                                          }% {t('Orders-Late Dispatched')}
                                       </small>
                                    </OverlayTrigger>
                                 </div>
                                 <div className={col4}>
                                    <ProgressBar
                                       className={`${style.progress}`}
                                       now={this.state.lateDispatchedPercent}
                                       // label={`${this.now[2]}%`}
                                    />
                                 </div>
                              </div>
                              <hr></hr>
                              <Card.Body>
                                 <div className="row">
                                    <PieClass
                                       lang={this.props.language}
                                       update={this.state.chartUpdate}
                                       data={
                                          this.state.statisticalData
                                             ? this.state.statisticalData
                                                  .cardTwo
                                             : []
                                       }
                                       colorData={[
                                          '#E54515',
                                          '#ad9551',
                                          '#1A6634',
                                       ]}
                                       legendRef={this.doRef}
                                    />
                                 </div>
                              </Card.Body>
                           </Card>
                        </div>
                     </div>
                     <div className="row mt-1">
                        <div className={col12}>
                           <Card>
                              <div
                                 className={`${
                                    lang === LANG_AR && 'text-right'
                                 } row no-gutters`}
                                 dir={lang === LANG_AR ? 'rtl' : null}
                              >
                                 {/* <div className="col-12 custom-text-font">
                                 <div className="row mt-1 mr-1"> */}
                                 <div
                                    className={`${col4} ${
                                       lang === LANG_AR ? 'mr-2' : 'ml-2'
                                    } `}
                                 >
                                    <small className={style.textFontSize}>
                                       {t('Orders Not Completed')}
                                    </small>
                                 </div>
                                 <div className={` ${col7} text-right`}>
                                    <small className={style.textFontSize}>
                                       {this.state.ordersNotCompleted}
                                    </small>
                                 </div>
                              </div>
                              <div
                                 className={`${
                                    lang === LANG_AR && 'text-right'
                                 } row`}
                                 dir={lang === LANG_AR ? 'rtl' : null}
                              >
                                 <div
                                    className={`${col4} ${
                                       lang === LANG_AR ? 'mr-2' : 'ml-2'
                                    } `}
                                 >
                                    <OverlayTrigger
                                       key={1}
                                       placement={'top'}
                                       overlay={
                                          <Tooltip
                                             id={`tooltip-123456`}
                                             style={{ fontSize: '10px' }}
                                          >
                                             {t('Orders-Other Reasons')}
                                          </Tooltip>
                                       }
                                    >
                                       <small className={style.textFontSize}>
                                          0% {t('Orders-Other Reasons')}
                                       </small>
                                    </OverlayTrigger>
                                 </div>
                                 <div className={col7}>
                                    <ProgressBar
                                       className={`${style.progress}`}
                                       now={this.now[1]}
                                    />
                                 </div>
                              </div>
                              <hr></hr>
                              <Card.Body>
                                 <div className="row">
                                    <PieClass
                                       update={this.state.chartUpdate}
                                       data={
                                          this.state.statisticalData
                                             ? this.state.statisticalData
                                                  .cancelStatistics
                                             : []
                                       }
                                       colorData={[
                                          '#E54515',
                                          '#EF9B5',
                                          '#F99D2E',
                                          '#d19e2b',
                                          '#ad9551',
                                          '#866417',
                                          '#39C86A',
                                          '#1A6634',
                                          '#2EA6F9',
                                          '#E51541',
                                       ]}
                                       legendRef={this.oncRef}
                                       lang={this.props.language}
                                    />
                                 </div>
                              </Card.Body>
                           </Card>
                        </div>
                     </div>
                     <div className="row mt-1">
                        <div className={col6}>
                           <b>
                              <Trans i18nKey={'Fleet Availability'} />
                           </b>
                        </div>
                     </div>
                     <div className="row mt-1">
                        <div className={col6}>
                           <Card>
                              <div
                                 className={`${
                                    lang === LANG_AR && 'text-right'
                                 } row mt-1`}
                                 dir={lang === LANG_AR ? 'rtl' : null}
                              >
                                 <div
                                    className={`${col6} ${
                                       lang === LANG_AR ? `mr-2` : `ml-2`
                                    } `}
                                 >
                                    <small className={style.textFontSize}>
                                       {t('Delivery Resources')}
                                    </small>
                                 </div>
                              </div>
                              <hr></hr>
                              <Card.Body>
                                 <div className="row">
                                    <PieClass
                                       update={this.state.chartUpdate}
                                       data={
                                          this.state.vehicleCategories.length >
                                          0
                                             ? this.state.vehicleCategories
                                             : []
                                       }
                                       colorData={[
                                          '#ad9551',
                                          '#1A6634',
                                          '#E54515',
                                       ]}
                                       legendRef={this.osRef}
                                       lang={this.props.language}
                                    />
                                 </div>
                              </Card.Body>
                           </Card>
                        </div>
                        <div className={col6}>
                           <Card>
                              <div
                                 className={`${
                                    lang === LANG_AR && 'text-right'
                                 } row mt-1`}
                                 dir={lang === LANG_AR ? 'rtl' : null}
                              >
                                 <div
                                    className={`${col6} ${
                                       lang === LANG_AR ? `mr-2` : `ml-2`
                                    } `}
                                 >
                                    <small className={style.textFontSize}>
                                       {t('Vehicles')}
                                    </small>
                                 </div>
                              </div>
                              <hr></hr>
                              <Card.Body>
                                 <div className="row">
                                    <PieClass
                                       update={this.state.chartUpdate}
                                       data={
                                          this.state.liveVehiclesStats.length >
                                          0
                                             ? this.state.liveVehiclesStats
                                             : []
                                       }
                                       colorData={[
                                          '#1A6634',
                                          '#ad9551',
                                          '#E54515',
                                       ]}
                                       legendRef={this.vRef}
                                       lang={this.props.language}
                                    />
                                 </div>
                              </Card.Body>
                           </Card>
                        </div>
                     </div>
                     <br></br>
                  </div>
                  <div className={col6}>
                     {navigator.onLine ? (
                        <Map
                           language={this.props.language}
                           vehicleTrackingData={this.state.vehicleTracking}
                           routelist={[]}
                           t={this.props.t}
                           zoom={10}
                           // mapfeatures={this.state.mapfeatures}
                           googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${process.env.REACT_APP_GOOGLE_KEY}&language=en}`}
                           loadingElement={<div style={{ height: '80vh' }} />}
                           containerElement={<div style={{ height: '80vh' }} />}
                           mapElement={<div style={{ height: '80vh' }} />}
                        />
                     ) : (
                        <h1 style={{ margin: '25%' }}>
                           No Internet Connection
                        </h1>
                     )}
                  </div>
               </div>
            </div>
         </React.Fragment>
      )
   }
}

export default Controltower
