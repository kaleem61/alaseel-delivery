import React, { createRef, Component } from 'react'
import d3Tip from 'd3-tip'
import * as d3Modules from 'd3'
import style from './PieClass.module.css'
import { LANGUAGE_STRING } from '../Constants/Enviroment/Enviroment'
// import { tip } from 'd3'
import _ from 'lodash'
import {
   col7,
   col8,
   col12,
   col11,
   col6,
   col5,
   col9,
   col10,
} from '../Constants/Classes/BoostrapClassses'
import { thresholdFreedmanDiaconis } from 'd3'
const d3 = {
   ...d3Modules,
   tip: d3Tip,
}
class PieClass extends Component {
   constructor(props) {
      super(props)
      this.myRef = createRef()
      this.legRef = createRef()
      this.sate = {
         screenWidth: window.innerWidth,
      }
   }
   createPie = (newdata) => {
      let totalCount = 0
      let newtotalCount = 0
      let data = [...newdata]
      data.map((d) => {
         totalCount = totalCount + parseInt(d.count)
      })

      if (totalCount === 0 && data.length > 0) {
         data.push({
            count: 20,
            status: { en: 'default', ar: 'default' },
         })
         data.map((d) => {
            totalCount = totalCount + parseInt(d.count)
         })
      }
      const div = d3.select(this.myRef.current)
      div.selectAll('*').remove()

      const width = 90
      const height = 90
      const radius = Math.min(width, height) / 2.2
      let colors = []
      if (typeof this.props.colorData !== 'undefined') {
         colors = d3.scaleOrdinal(this.props.colorData)
      } else {
         colors = d3.scaleOrdinal([
            '#ff0000',
            '#433b33',
            '#d19f2b',
            '#1a6734',
            ...d3.schemeSet1,
         ])
      }

      let tip = d3
         .tip()
         .attr('class', style.d3Tip)
         .style('font-size', '10px')

         .html(({ data }) => {
            let currentPercentage = (parseInt(data.count) * 100) / totalCount
            return `<strong> ${currentPercentage.toFixed(2)}%</strong>`
         })

      let legendtip = d3
         .tip()
         .attr('class', style.d3Tip)
         .style('font-size', '10px')

         .html((data) => {
            let currentPercentage = (parseInt(data.count) * 100) / totalCount
            return `<strong> ${
               data.status[this.props.lang]
            }${' '}${currentPercentage.toFixed(2)}%</strong>`
         })

      const svg = div
         .append('svg')
         .attr('width', '100%')
         .attr('height', '100%')
         .append('g')
         .style('cursor', 'pointer')
         //   .attr('class','arc')
         .attr('transform', `translate(${width / 1.9},${height / 2})`)
      svg.call(tip)

      const pie = d3
         .pie()
         .value((d) => {
            return d.count
         })
         .sort(null)
      const arc = d3
         .arc()
         .outerRadius(radius * 0.75)
         .innerRadius(radius)

      const hoverArc = d3
         .arc()
         .outerRadius(radius * 0.85)
         .innerRadius(radius * 1.1)
      const labelArc = d3
         .arc()
         .innerRadius(radius / 1.4)
         .outerRadius(radius)
      const g = svg
         .selectAll('.arc')
         .data(pie(data))
         .enter()
         .append('g')
         .attr('class', 'arc')

      g.append('path')
         .attr('d', arc)
         .attr('class', 'arc')
         .style('fill', (d, i) => {
            if (d.data.status[this.props.lang] === 'default') {
               return 'white'
            } else {
               return colors(d.index)
            }
         })
         // .style('fill-opacity', 0.7)
         .style('stroke', '#11141C')
         .style('stroke-width', 0.5)
         .on('mouseover', (data, index, element) =>
            tip.show(data, element[index])
         )
         .on('mouseout', function (d, i) {
            tip.hide()
         })
      let legdiv = d3.select(this.legRef.current)
      legdiv.selectAll('*').remove()
      let legsvg = legdiv
         .append('svg')
         .attr('width', '100%')
         .attr('height', '100%')
         // .style('height', 'fit-content')
         .append('g')
         .attr('transform', `translate(${1},${-20})`)

      var legendG = legsvg
         .selectAll('.legends')
         .data(
            _.filter(data, (o) => {
               return o.status[this.props.lang] !== 'default'
            })
         )
         .style('fill-opacity', 2)
         .enter()
         .append('g')
         .attr('transform', (d, i) => {
            return 'translate(' + width * 0.1 + ',' + (i * 15 + 20) + ')'
         })
         .attr('class', 'legends')
      legendG.call(legendtip)
      legendG
         .append('rect')
         .attr('width', 10)
         .attr('height', 10)
         .attr('fill', function (d, i) {
            return colors(i)
         })
      legendG
         .append('text')
         .text((data) => {
            return `${data.count}-${
               typeof data.status[this.props.lang] === 'undefined'
                  ? data.status.en
                  : data.status[this.props.lang]
            }`
         })

         // .style('font-size', 12)
         .attr('y', 10)
         .attr('x', 14)
         .style('cursor', 'pointer')
         .on('mouseover', (data, index, element) =>
            legendtip.show(data, element[index])
         )
         .on('mouseout', function (d, i) {
            legendtip.hide()
         })
         .on('zoom', () => {
            console.log('Check Whelle')
         })
   }
   componentDidUpdate(prevProps, prevState) {
      if (
         prevProps.data.length !== this.props.data.length ||
         this.props.lang !== prevProps.lang ||
         this.props.update !== prevProps.update ||
         (this.state &&
            prevState &&
            this.state.screenWidth !== prevState.screenWidth)
      ) {
         this.createPie(this.props.data)
      }
   }
   componentDidMount() {
      if (this.props.data.length > 0) {
         this.createPie(this.props.data)
      }
      window.addEventListener('resize', this.resize.bind(this))
      this.resize()
   }
   resize = () => {
      this.setState({
         screenWidth: window.innerWidth,
      })
   }
   render() {
      return (
         <React.Fragment>
            {this.state && this.state.screenWidth > 768 ? (
               <div className={col12}>
                  <div className="row">
                     <div
                        style={{ fontSize: '10px' }}
                        className={col7}
                        ref={this.legRef}
                     ></div>
                     <div className={col5} ref={this.myRef}></div>
                  </div>
               </div>
            ) : (
               <div className={col12}>
                  <div className="row">
                     <div className={col12} ref={this.myRef}></div>
                  </div>
                  <div className="row">
                     <div
                        style={{ fontSize: '10px' }}
                        className={col12}
                        ref={this.legRef}
                     ></div>
                  </div>
               </div>
            )}
         </React.Fragment>
      )
   }
}

export default PieClass
