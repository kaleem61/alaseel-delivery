import React, { Component } from 'react'
import NavBar from '../NavBar/NavBar'
import '../../css/mapstyling.css'
import { useLocation } from 'react-router-dom'
import NotFound from '../exceptions/NotFound'
import Live from '../live/Live'
import ControlTower from '../controltower/Controltower'
import RoutePlan from '../routesplan/RoutesPlan'
import { withRouter } from 'react-router-dom'
import { ToastContainer, toast, Zoom } from 'react-toastify'
import { withCookies } from 'react-cookie'

import {
   LoadFadeLoader,
   LoadCircleLoader,
   LoadClipLoader,
} from '../Loaders/Loaders'
import {
   BrowserRouter as Router,
   Switch,
   Route,
   Link,
   NavLink,
   Redirect,
} from 'react-router-dom'
import Login from './Login'
import './App.css'
import { Trans, withTranslation, useTranslation } from 'react-i18next'
import style from './Login.module.css'
import { askForPermissioToReceiveNotifications } from '../notifications/Push'
import { LANGUAGE_STRING } from '../Constants/Enviroment/Enviroment'
class App extends Component {
   constructor(props) {
      super(props)
      this.myRef = React.createRef()
      this.state = {
         timeSlots: null,
         cancalReasons: null,
         vehicles: null,
         vehiclesdata: null,
         vehicleRoutes: null,
         storeList: [],
         currentlocation: null,
         loggedIn: false,
         selectedWareHouseId: null,
         isLoading: false,
         defaultCenter: null,
         routesEnabled: false,
         language: '',
      }
   }
   static getDerivedStateFromProps(props, state) {
      let location = props.location.pathname
      if (
         localStorage.getItem('authtoken') &&
         localStorage.getItem('username') &&
         props.location.pathname !== '/login'
      ) {
         return {
            loggedIn: true,
         }
      } else {
         if (localStorage.getItem('authtoken')) {
            localStorage.removeItem('authtoken')
         }
         if (localStorage.getItem('username')) {
            localStorage.removeItem('username')
         }
         return {
            loggedIn: false,
            selectedWareHouseId: null,
         }
      }
      return {
         currentlocation: location,
      }
   }
   checkLoggedIn = (status) => {
      if (status) {
         this.setState({
            loggedIn: status,
         })
         this.showMessage('Logged In Successfully', 'success')
      }
   }
   renderNavBar = () => {
      return <NavBar></NavBar>
   }
   callbackFunction = (childData, date) => {
      this.setState({ storeList: childData })
   }
   getCurrentDate = (date) => {
      this.setState({
         currentDate: date,
         vehicles: null,
      })
   }
   getVehiclesList = (vehicles) => {
      this.setState({
         vehicles: vehicles,
      })
   }
   componentDidMount() {
      const { cookies } = this.props
      cookies.set('LANGUAGE', localStorage.getItem(LANGUAGE_STRING), {
         path: '/',
      })
      this.setState({ isLoading: false, language: this.props.i18n.language })
   }
   componentDidUpdate(prevProps, prevState) {
      if (this.state.language !== this.props.i18n.language) {
         const { cookies } = this.props
         cookies.set('LANGUAGE', this.props.i18n.language, {
            path: '/',
         })
         this.setState({
            language: this.props.i18n.language,
         })
      }
      if (prevState.storeList !== this.state.storeList) {
         this.setState({
            vehiclesdata: null,
         })
      }
      if (prevState.vehicles !== this.state.vehicles) {
         this.setState({ vehiclesdata: this.state.vehicles })
      }
   }

   selectedWareHouseId = (warehouse_id, defaultCenter) => {
      this.setState({
         selectedWareHouseId: warehouse_id,
         defaultCenter: defaultCenter,
      })
   }
   getRouteStatus = (status) => {
      this.setState({
         routesEnabled: status,
      })
   }
   showMessage = (message, type, autoClose = 2000) =>
      toast(message, {
         type: type,
         // autoClose: false,
         autoClose: autoClose,
         className:
            type === 'success'
               ? style.toastContainerSuccess
               : style.toastContainer,
         // progressClassName: style.toastProgressBar,
      })
   render() {
      return (
         <div className="container-fluid">
            <ToastContainer
               position="top-center"
               transition={Zoom}
               // autoClose={1500}
               hideProgressBar={false}
               newestOnTop={false}
               closeOnClick
               rtl={false}
               pauseOnVisibilityChange
               draggable
               pauseOnHover
            />
            <LoadClipLoader
               css={`
                  filter: blur(8px);
                  position: fixed;
                  top: 40%;
                  left: 40%;
                  right: 40%;
                  bottom: 20%;

                  opacity: 0.5;
                  z-index: 999999;
               `}
               size={300}
               loading={this.state.isLoading}
            ></LoadClipLoader>
            <Switch>
               <Route exact path="/test">
                  <Trans i18nKey={'hello.label'}>{'hello.label'}</Trans>
                  <button onClick={() => this.props.i18n.changeLanguage('ar')}>
                     Test Firebase
                  </button>
               </Route>
               {this.state.loggedIn ? (
                  <React.Fragment>
                     <Route exact path="/">
                        <NavBar
                           {...this.props}
                           location={this.props.location.pathname}
                           warehouse_id={this.selectedWareHouseId}
                           selectedwarehouse_id={this.state.selectedWareHouseId}
                           vehiclesList={this.getVehiclesList}
                           setRoutesStatus={this.getRouteStatus}
                        ></NavBar>
                        <RoutePlan
                           t={this.props.t}
                           language={localStorage.getItem(LANGUAGE_STRING)}
                           routeStatus={this.state.routesEnabled}
                           defaultCenter={this.state.defaultCenter}
                           selectedwarehouse_id={this.state.selectedWareHouseId}
                        />
                     </Route>
                     <Route path="/live">
                        <NavBar
                           {...this.props}
                           location={this.props.location.pathname}
                           warehouse_id={this.selectedWareHouseId}
                           // currentDate={this.state.currentDate}
                           selectedwarehouse_id={this.state.selectedWareHouseId}
                           vehiclesList={this.getVehiclesList}
                           setRoutesStatus={this.getRouteStatus}
                        ></NavBar>
                        <Live
                           t={this.props.t}
                           language={localStorage.getItem(LANGUAGE_STRING)}
                           routeStatus={this.state.routesEnabled}
                           defaultCenter={this.state.defaultCenter}
                           selectedwarehouse_id={this.state.selectedWareHouseId}
                           vehicles={this.state.vehiclesdata}
                           parentCallback={this.callbackFunction}
                           currentDateCallBack={this.getCurrentDate}
                        />
                     </Route>

                     <Route path="/controltower">
                        <NavBar
                           {...this.props}
                           location={this.props.location.pathname}
                           warehouse_id={this.selectedWareHouseId}
                           selectedwarehouse_id={this.state.selectedWareHouseId}
                           vehiclesList={this.getVehiclesList}
                           setRoutesStatus={this.getRouteStatus}
                        ></NavBar>
                        <ControlTower
                           t={this.props.t}
                           language={localStorage.getItem(LANGUAGE_STRING)}
                           defaultCenter={this.state.defaultCenter}
                           routeStatus={this.state.routesEnabled}
                           setSelectedDate={this.getSelectedDate}
                           selectedwarehouse_id={this.state.selectedWareHouseId}
                        />
                     </Route>
                     {/* <Route path="/routesplan">
                        <NavBar
                           location={this.props.location.pathname}
                           warehouse_id={this.selectedWareHouseId}
                           selectedwarehouse_id={this.state.selectedWareHouseId}
                           vehiclesList={this.getVehiclesList}
                        ></NavBar>
                        <RoutePlan
                           selectedwarehouse_id={this.state.selectedWareHouseId}
                        />
                     </Route> */}
                  </React.Fragment>
               ) : (
                  <React.Fragment>
                     <Route exact path="/login">
                        <Login loggedin={this.checkLoggedIn} />
                     </Route>
                     <Redirect to="/login"></Redirect>
                  </React.Fragment>
               )}
            </Switch>
         </div>
      )
   }
}

export default withCookies(withTranslation()(withRouter(App)))
