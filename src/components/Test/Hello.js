import React from 'react'
import { Trans, useTranslation } from 'react-i18next'

const Hello = () => {
   const { t, i18n } = useTranslation()

   return (
      <div>
         <Trans>{'hello.label'}</Trans>
      </div>
   )
}

export default Hello
