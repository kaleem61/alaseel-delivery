import React from 'react'
import { withTranslation ,Trans} from 'react-i18next'

const ThankYou = () => {
   return <Trans i18nKey={'thankyou.label'}>{'thankyou.label'}</Trans>
}

export default withTranslation()(ThankYou)
