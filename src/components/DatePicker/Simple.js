import React, { Component } from 'react'
import DatePicker from 'react-datepicker'

class Simple extends Component {
   constructor(props) {
      super(props)
      this.state = {
         showTimeSelect: false,
      }
   }
   render() {
      return (
         
         <DatePicker
            showTimeSelect={this.props.showTimeSelect}
            title={this.props.title}
            selected={this.props.currentDate}
            dateFormat={this.props.dateFormat}
            onChange={this.props.onChange}
            className={this.props.className ? this.props.className : '' }
            containerClassName={`row`}
            calendarClassName={this.props.calendarClassName}
            popperPlacement="top-start"
            minDate={this.props.minDate}
         ></DatePicker>
      )
   }
}
export default Simple
