import React, { Component } from 'react'
import {
   Modal,
   Container,
   Row,
   Col,
   Button,
   Form,
   FormGroup,
} from 'react-bootstrap'
import { FadeLoader } from 'react-spinners'
import $ from 'jquery'
import axios from '../API/Axios'
import style from './CreateTripModal.module.css'
import { ToastContainer, toast, Zoom } from 'react-toastify'
import { LOCAL_API_URL } from '../Constants/Enviroment/Enviroment'
import { LANG_AR } from '../Constants/Language/Language'
class CancleReasonsModal extends Component {
   constructor(props) {
      super(props)
      this.state = {
         validated: false,
         reasons: [],
         order_id: null,
      }
   }
   renderFadeLoader = () => {
      return (
         <FadeLoader
            css={`
               cssdisplay: block;
               margin: 0 auto;
               border-color: red;
            `}
            size={150}
            color={'#123abc'}
            loading={this.state.sideloading}
         />
      )
   }
   static getDerivedStateFromProps(props, state) {
      if (props.reasons && props.orderid) {
         return {
            reasons: props.reasons.cancel_reasons,
            order_id: props.orderid,
         }
      }
      return state
   }
   showMessage = (message, type, autoClose = 2000) =>
      toast(message, {
         type: type,
         // autoClose: false,
         autoClose: autoClose,
         className:
            type === 'success'
               ? style.toastContainerSuccess
               : style.toastContainer,
      })
   handleSubmit = (e) => {
      const formdata = new FormData(e.target)

      const data = {
         order_id: this.state.order_id,
         cancel_reason_id: this.refs.cancelreason.value,
      }
      const newdata = JSON.stringify(data)
      axios
         .post(`storesupervisor/v1/cancelOrders`, newdata, {
            headers: {
               Authorization: `bearer ${localStorage.getItem('authtoken')}`,
            },
         })
         .then((res) => {
            let response = res.data
            if (response.code === 200) {
               this.showMessage(response.message, 'success', false)
            } else {
               this.showMessage(response.message, 'error', false)
            }
         })
         .catch((error) => console.log(error))
      e.preventDefault()
   }
   render() {
      let t = this.props.t
      return (
         <Modal
            show={this.props.show}
            onHide={this.props.onHide}
            aria-labelledby="contained-modal-title-vcenter"
         >
            <Form
               noValidate
               validated={this.state.validated}
               onSubmit={(e) => this.handleSubmit(e)}
            >
               <Modal.Header closeButton>
                  <Modal.Title id="contained-modal-title-vcenter">
                     {t('Cancel Order Reasons')}
                  </Modal.Title>
               </Modal.Header>
               <Modal.Body>
                  <Container>
                     {this.state.reasons.length > 0
                        ? this.state.reasons.map((reason, key) => (
                             <Row
                                key={reason.cancel_reason_id}
                                className="show-grid"
                             >
                                <Col xs={6} md={10}>
                                   <FormGroup>
                                      <Form.Check
                                         type={`radio`}
                                         ref="cancelreason"
                                         value={reason.cancel_reason_id}
                                         name="cancelReason"
                                         id={`default ${reason.cancel_reason_id}`}
                                         label={reason.reason.en}
                                      />
                                   </FormGroup>
                                </Col>
                             </Row>
                          ))
                        : ''}
                  </Container>
               </Modal.Body>
               <Modal.Footer>
                  <Button type="submit" className="btn btnGreen ">
                     {t('Save')}
                  </Button>
                  <Button className="btn btnBrown" onClick={this.props.onHide}>
                     {t('Close')}
                  </Button>
               </Modal.Footer>
            </Form>
         </Modal>
      )
   }
}

export default CancleReasonsModal
