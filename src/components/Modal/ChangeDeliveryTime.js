import React, { Component } from 'react'
import {
   Modal,
   Container,
   Row,
   Col,
   Button,
   Form,
   FormGroup,
} from 'react-bootstrap'
import { FadeLoader } from 'react-spinners'
import axios from '../API/Axios'
import DatePicker from '../DatePicker/Simple'
import moment from 'moment'
import style from './CreateTripModal.module.css'
import { ToastContainer, toast, Zoom } from 'react-toastify'
import { LOCAL_API_URL } from '../Constants/Enviroment/Enviroment'
import { LANG_AR, LANG_EN } from '../Constants/Language/Language'
class ChangeDeliveryTime extends Component {
   constructor(props) {
      super(props)
      this.state = {
         validated: false,
         timeSlots: [],
         order_id: null,
         currentDate: new Date(),
         dateFormat: 'yyyy-MM-dd',
         currentTimeSlot: null,
         selectedTimeSlot: null,
         showModal: false,
      }
   }
   renderFadeLoader = () => {
      return (
         <FadeLoader
            css={`
               cssdisplay: block;
               margin: 0 auto;
               border-color: red;
            `}
            size={150}
            color={'#123abc'}
            loading={this.state.sideloading}
         />
      )
   }
   static getDerivedStateFromProps(props, state) {
      if (props.timeslots && props.orderid && props.currenttimeslot) {
         if (state.selectedTimeSlot) {
            return {
               showModal: true,
               currentTimeSlot: state.selectedTimeSlot,
               timeSlots: props.timeslots,
               order_id: props.orderid,
            }
         } else {
            return {
               showModal: true,
               currentTimeSlot: props.currenttimeslot,
               timeSlots: props.timeslots,
               order_id: props.orderid,
            }
         }
      }
      return state
   }
   handleSubmit = (e) => {
      const formData = new FormData(e.target)
      let deliverytime = formData.get('changedelivery')
      let data = {
         order_id: this.state.order_id,
         delivery_slot_id: deliverytime,
         date: moment(this.state.currentDate).format('YYYY-MM-DD'),
      }

      axios
         .post(`storesupervisor/v1/updateOrderDelivery`, JSON.stringify(data), {
            headers: {
               Authorization: `bearer ${localStorage.getItem('authtoken')}`,
            },
         })
         .then((res) => {
            let response = res.data
            if (response.code === 200) {
               this.showMessage(response.message, 'success', false)
               this.setState({ selectedTimeSlot: null })
               this.props.onHide(false)
               this.props.changedeliveryslotid(
                  this.state.order_id,
                  deliverytime
               )
            } else {
               this.showMessage(response.message, 'error', false)
            }
         })
         .catch((error) => console.log(error))
      e.preventDefault()
   }
   showMessage = (message, type, autoClose = 2000) =>
      toast(message, {
         type: type,
         // autoClose: false,
         autoClose: autoClose,
         className:
            type === 'success'
               ? style.toastContainerSuccess
               : style.toastContainer,
      })
   handleDateChange = (date) => {
      this.setState({
         currentDate: date,
      })
   }
   handleRadioChange = (e) => {
      this.setState({
         selectedTimeSlot: parseInt(e.target.value),
      })
   }
   render() {
      let t = this.props.t
      let lang = this.props.language
      return (
         <Modal
            show={this.props.show}
            onHide={this.props.onHide}
            aria-labelledby="contained-modal-title-vcenter"
         >
            <Form
               noValidate
               validated={this.state.validated}
               onSubmit={(e) => this.handleSubmit(e)}
            >
               <Modal.Header closeButton>
                  <Modal.Title id="contained-modal-title-vcenter">
                     {t('Change Delivery Time')}
                  </Modal.Title>
               </Modal.Header>
               <Modal.Body>
                  <Container>
                     <Row className="show-grid align-items-center">
                        <Col>
                           <Row>
                              <Form.Label>{t('Change Date')}</Form.Label>
                           </Row>
                           <Row>
                              <Col xs={6} md={10}>
                                 <FormGroup as={Row}>
                                    <DatePicker
                                       showTimeSelect={false}
                                       title={t('Select Date')}
                                       currentDate={this.state.currentDate}
                                       dateFormat={this.state.dateFormat}
                                       onChange={this.handleDateChange}
                                    ></DatePicker>
                                 </FormGroup>
                              </Col>
                           </Row>
                        </Col>
                     </Row>
                     <Row className="show-grid">
                        <Form.Label>{t('Change Time Slot')}</Form.Label>
                        {this.state.timeSlots.length > 0
                           ? this.state.timeSlots
                                .sort((a, b) => a.slot_id < b.slot_id)
                                .map((slot, key) => (
                                   <Col key={key} xs={6} md={10}>
                                      <FormGroup as={Row}>
                                         <Form.Check
                                            type={`radio`}
                                            ref={`changedelivery${key}`}
                                            checked={
                                               slot.slot_id ===
                                               this.state.currentTimeSlot
                                            }
                                            onChange={this.handleRadioChange}
                                            value={slot.slot_id}
                                            name="changedelivery"
                                            id={`default${key}`}
                                            label={slot.tile.en}
                                         />
                                      </FormGroup>
                                   </Col>
                                ))
                           : null}
                     </Row>
                  </Container>
               </Modal.Body>
               <Modal.Footer>
                  <Button type="submit" className="btn btnGreen">
                     {t('Update')}
                  </Button>
                  <Button className="btn btnBrown" onClick={this.props.onHide}>
                     {t('Close')}
                  </Button>
               </Modal.Footer>
            </Form>
         </Modal>
      )
   }
}

export default ChangeDeliveryTime
