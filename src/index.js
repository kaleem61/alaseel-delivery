import React, { Suspense } from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import { initializeFirebase } from './components/notifications/Push'
import axios from 'axios'

import {
   BrowserRouter as Router,
   Switch,
   Route,
   Link,
   useLocation,
} from 'react-router-dom'
import { I18nextProvider } from 'react-i18next'
import i18n from './i18n'
import './i18n'
import App from './components/Containers/App'
import { CookiesProvider } from 'react-cookie'
import * as serviceWorker from './serviceWorker'
import ErrorBoundary from './components/ErrorHandler/ErrorBoundary'
import logo from './images/controltower.png'
import {
   CURRENT_ENVIREMENT,
   IN_PRODUCTION_MODE,
   PRODUCTION_BASE_URL,
   IN_STAGGING_MODE,
   STAGGING_BASE_URL,
   DEVELOPMENT_BASE_URL,
} from './components/Constants/Enviroment/Enviroment'
console.log('ENVIREMONT', process.env)
ReactDOM.render(
   // <TestApp/>,
   <Suspense fallback={null}>
      <ErrorBoundary>
         <CookiesProvider>
            <Router
               basename={
                  CURRENT_ENVIREMENT === IN_PRODUCTION_MODE
                     ? PRODUCTION_BASE_URL
                     : CURRENT_ENVIREMENT === IN_STAGGING_MODE
                     ? STAGGING_BASE_URL
                     : DEVELOPMENT_BASE_URL
               }
            >
               <App />
            </Router>
         </CookiesProvider>
      </ErrorBoundary>
   </Suspense>,
   document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
initializeFirebase()
// AMQB()
// Sockets()
// MQTT()
// connectRabbit()
