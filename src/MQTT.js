import { connect } from 'mqtt'
import {
   Paho,
   Client,
   Message,
   onConnect,
   onConnectionLost,
   onMessageArrived,
} from 'paho-mqtt'
import * as amqp from 'amqplib'
import React, { Component } from 'react'
import { w3cwebsocket as W3CWebSocket } from 'websocket'
import * as cakey from '../src/assets/client_key.pem'
import * as clinetCA from '../src/assets/client_certificate.pem'
import * as CA from '../src/assets/ca_certificate.pem'
import mqtt from 'mqtt'
// const client = connect('ws://test.mosquitto.org')

export const MQTT = () => {
   /* working code */
   // MQTTCODE()
   // TestWebSocket()
   // AMQB()
   // PAHOFUNC() // update working and best func for WS
   normalMqtt()
}
const PAHOFUNC = () => {
   let client = new Client(
      'gbhcdemosys.com',
      Number(15675),
      '/ws',
      'mqttjs_' + Math.random().toString(16).substr(2, 8)
   )
   // set callback handlers
   client.connect({
      onSuccess: () => {
         client.subscribe('111')
         // let message = new Message('Test Hello Word Kaleem 121212121')
         // message.destinationName = 'World'
         // // client.publish(message)
         // client.send(message)
      },
      userName: 'admin_aseel',
      password: 'AlAseel!@#',
      mqttVersion: 4,
      useSSL: true,
      // keyPath: cakey,
      // certPath: '',
      //           certPath: '/var/www/html/test02/client-cert.pem',
      useTLS: true,
      // tls: true,
      rejectUnauthorized: false,
      ca: ['/var/www/html/test02/cacert.pem'],
      protocol: 'wss',
      //   mqttVersion: 4,
      cleanSession: false,
      // keepAliveInterval: 1500,
      // timeout: 15000,
      // reconnectInterval: 10,
      // willMessage: {  },
      onFailure: (message) => {
         console.log('check will Failutre messgae message', message)
      },
      keepAliveInterval: 1500,
      reconnect: true, // Enable automatic reconnect
      timeout: 15000,
   })
   client.onConnectionLost = (responseObject) => {
      if (responseObject.errorCode !== 0) {
         console.log('onConnectionLost:' + responseObject.errorMessage)
      }
   }
   client.onMessageArrived = (testmessage) => {
      console.log('onMessageArrived:' + testmessage.payloadString)
      // client.subscribe('World')
      // let message = new Message('Hello Test Word')
      // message.destinationName = 'World'
      // client.send(message)
      // client.reconnect()
   }
   client.onConnect = (message) => {
      console.log('check test log', message)
   }
   client.onConnected = (resObj) => {
      console.log('Connected to', resObj)
      // client.subscribe('World')
   }
   // connect the client

   // called when the client connects
   // const onConnect = () => {
   //    // Once a connection has been made, make a subscription and send a message.
   //    console.log('onConnect Yup')
   //    client.subscribe('World')
   //    let message = new Message('Hello Test Word')
   //    message.destinationName = 'World'
   //    client.send(message)
   // }
   // // called when the client loses its connection
   // onConnectionLost = (responseObject) => {
   //    if (responseObject.errorCode !== 0) {
   //       console.log('onConnectionLost:' + responseObject.errorMessage)
   //    }
   // }
   // called when a message arrives
   // onMessageArrived = (message) => {
   //    console.log('onMessageArrived:' + message.payloadString)
   // }
}

const normalMqtt = () => {
   let options = {
      keepalive: 60,
      clientId: 'mqttjs_' + Math.random().toString(16).substr(2, 8),
      protocolId: 'MQTT',
      protocolVersion: 4,
      clean: true,
      reconnectPeriod: 1000,
      connectTimeout: 30 * 1000,
      rejectUnauthorized: false,
      userName: 'admin_aseel',
      password: 'AlAseel!@#',
      host: 'gbhcdemosys.com',
      port: '15676',
      path: '/ws',
      useSSL: true,
      // secureProtocol: 'TLSv1_method',
      protocol: 'wss',
      key: './ssl/client_key.pem',
      cert: './ssl/client_certificate.pem',
      passphrase: 'alaseel',
      ca: './ssl/ca_certificate.pem',
      // key: cakey,
      // cert: clinetCA,
      // ca: CA,
   }
   console.log('Check Options', options)
   var client = mqtt.connect(options)

   client.on('connect', function () {
      client.subscribe('1')
      // if (!err) {
      //    client.publish('presence', 'Hello mqtt')
      // }
      // })
   })

   client.on('message', function (topic, message) {
      // message is Buffer
      console.log(message.toString())
      client.end()
   })
}
